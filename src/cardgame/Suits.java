package cardgame;

public enum Suits {

    HEART("Hearts"),
    DIAMOND("Diamonds"),
    CLUB("Clubs"),
    SPADE("Spades");

    private String suit;
    
    Suits(String suit) {
        this.suit = suit;
    }

    public String getSuit() {
        return suit;
    }
    
    @Override
    public String toString() {
        return this.suit;
    }
}
