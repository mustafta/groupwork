package cardgame;

public class Cards {
    private Suits suits;
    private FaceCards rank;
    private int value;
    
    Deck d1 = new Deck();
    public Cards(Suits suits, FaceCards rank) {
        this.suits = suits;
        this.rank = rank;
    }

    public Suits getSuits() {
        return suits;
    }

    public FaceCards getRank() {
        return rank;
    }

    public int getValue() {
        return this.rank.getValue();
    }
    
    @Override
    public String toString() {
        String new1 = "";
        if(FaceCards.ACE == getRank()){
            new1="Ace";
        }
        else if(FaceCards.KING == getRank()){
            new1="King";
        }
        else if(FaceCards.QUEEN == getRank()){
            new1="Queen";
        }
        else if(FaceCards.JACK == getRank()){
            new1="Jack";
        }
        else if(FaceCards.TEN == getRank()){
            new1="Ten";
        }
        else if(FaceCards.NINE == getRank()){
            new1="Nine";
        }
        else if(FaceCards.EIGHT == getRank()){
            new1="Eight";
        }
        else if(FaceCards.SEVEN == getRank()){
            new1="Seven";
        }
        else if(FaceCards.SIX == getRank()){
            new1="Six";
        }
        else if(FaceCards.FIVE == getRank()){
            new1="Five";
        }
        else if(FaceCards.FOUR == getRank()){
            new1="Four";
        }
        else if(FaceCards.THREE == getRank()){
            new1="Three";
        }
        else if(FaceCards.TWO == getRank()){
            new1="Two";
        }
        String new2 = new1.concat(" of ");
        String new3 = "";
        
        if(Suits.SPADE == getSuits()) {
            new3 = new2.concat("Spades");
        }
        else if(Suits.HEART == getSuits()) {
             new3 = new2.concat("Hearts");
        }
        else if(Suits.CLUB == getSuits()) {
             new3 = new2.concat("Clubs");
        }
        else if(Suits.DIAMOND == getSuits()) {
             new3 = new2.concat("Diamonds");
        }
        return new3;
    }
}

