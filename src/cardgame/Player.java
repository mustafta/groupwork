package cardgame;

import java.util.Scanner;

public class Player {
    private String player_name1;
    private String player_name2;
    
    public Player() {
        
    }
    
    public Player(String player_name1, String player_name2) {
        this.player_name1 = player_name1;
        this.player_name2 = player_name2;
    }

    public String getPlayer_name1() {
        return player_name1;
    }

    public String getPlayer_name2() {
        return player_name2;
    }

    public void setPlayer_name1(String player_name1) {
        this.player_name1 = player_name1;
    }

    public void setPlayer_name2(String player_name2) {
        this.player_name2 = player_name2;
    }
    
    public void displayName() {
       Scanner scanner = new Scanner(System.in);
       System.out.println("Enter your Name Player1: ");
       
       player_name1 = scanner.nextLine();
       
       System.out.println("Enter your Name Player2: ");
       player_name2= scanner.nextLine();
              
       System.out.println("Player 1: " + player_name1);
       System.out.println("Player 2: " + player_name2);
       System.out.println("Let's Play!");
    }
    
}
