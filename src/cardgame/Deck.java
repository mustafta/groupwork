package cardgame;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/**
 *
 * @author rushil
 */
public class Deck {
     List<Cards> cardList = new ArrayList<Cards>();
     LinkedList<Cards> cardList1 = new LinkedList<Cards>();
     LinkedList<Cards> cardList2 = new LinkedList<Cards>();
    
    public void deck() {
        for(Suits s : Suits.values()) {
            for(FaceCards f : FaceCards.values()) {
                cardList.add(new Cards(s, f));
            }
        }
        Collections.shuffle(this.cardList);
    }

    public List<Cards> getCardList() {
        return cardList;
    }
    
    public LinkedList<Cards> getCardList1() {
        cardList1.addAll(cardList.subList(0, 25));
        return cardList1;
    }
    
    public LinkedList<Cards> getCardList2() {
        cardList2.addAll(cardList.subList(26, 52));
        return cardList2;
   }    
}
