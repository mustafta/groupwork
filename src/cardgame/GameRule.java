package cardgame;

import java.util.ArrayList;
import java.util.List;


public class GameRule {
   
   private Cards cards;
   private Suits suits;
   private FaceCards faceCards;
   
   public void game() {
       Player player = new Player();
       player.displayName();
       int playerOneCards = 26;
       int playerTwoCards = 26;
       int playerOneWins = 0;
       int playerTwoWins = 0;
       int playerOneWinsWars = 0;
       int playerTwoWinsWars = 0;
       
       Deck d1 = new Deck();
       d1.deck();
            for ( int i = 0; i < d1.cardList.size(); i++ ) {
            Cards deckPlayerOne = d1.getCardList1().pop();  //each player place one card face up
            Cards deckPlayerTwo = d1.getCardList2().pop();

            //display the face up card
            System.out.println(player.getPlayer_name1() + " plays " + deckPlayerOne.toString());
            System.out.println(player.getPlayer_name2() + " plays " + deckPlayerTwo.toString());
            if(deckPlayerOne.getValue() > deckPlayerTwo.getValue()) {
                System.out.println(player.getPlayer_name1() + " wins");
                playerOneWins++;
                playerOneCards++;
                playerTwoCards--;
            }
            else if(deckPlayerOne.getValue() < deckPlayerTwo.getValue()) {
                System.out.println(player.getPlayer_name2() + " wins");
                playerTwoWins++;
                playerOneCards--;
                playerTwoCards++;
            }
            else {
               System.out.println("War");
                ArrayList<Cards> new1 = new ArrayList<Cards>();
                ArrayList<Cards> new2 = new ArrayList<Cards>();
               for(int j = 0; j < 3; j++) {
                   if(d1.cardList1.size() == 0 || d1.cardList2.size() == 0 ){
                        break;
                   }
                    
                    new1.add(d1.cardList1.pop()); 
                    new2.add(d1.cardList2.pop());
               }
                if(new1.get(1).getValue() > new2.get(1).getValue()) {
                System.out.println(player.getPlayer_name1() + " wins");
                playerOneWins++;
                playerOneCards++;
                playerTwoCards--;
                playerOneWinsWars++;
            }
                else if(new1.get(1).getValue() < new2.get(1).getValue()) {
                System.out.println(player.getPlayer_name2() + " wins");
                playerTwoWins++;
                playerOneCards--;
                playerTwoCards++;
                playerTwoWinsWars++;
            }
                else {
                    System.out.println("War again");
                }
            }
        }
            System.out.println("Game Ends!");
            System.out.println();
            System.out.println(player.getPlayer_name1() + " has: " + playerOneCards + " Cards");
            System.out.println(player.getPlayer_name1() + " won: " + playerOneWinsWars + " rounds of war");
            System.out.println();
            System.out.println(player.getPlayer_name2() + " has: " + playerTwoCards + " Cards");
            System.out.println(player.getPlayer_name2() + " won: " + playerTwoWinsWars + " rounds of war");
            System.out.println();
            if(playerOneCards > playerTwoCards) {
                System.out.println(player.getPlayer_name1() + " won the game!!!");
            }
            else {
                System.out.println(player.getPlayer_name2() + " won the game!!!");
            }
   }
   
}
