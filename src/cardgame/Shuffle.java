package cardgame;

import java.util.ArrayList;
import java.util.List;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Random;
/**
 *
 * @author rushil
 */
public class Shuffle {
    Deck deck = new Deck();
    
    public void shuffle() {
        for(int i = 0; i < deck.getCardList().size(); i++) {
            Collections.shuffle(deck.getCardList());
        }
    }

}
